import { DarkTheme }  from '@react-navigation/native';

export default Theme = {
    ...DarkTheme,
    colors: {
      ...DarkTheme.colors,
      background:   '#4f5b62',
      border:       "rgb(39, 39, 41)",
      card:         '#263238',
      notification: "rgb(255, 69, 58)",
      //primary: '#263238',
      text: '#e2f1f8',
    },
  };