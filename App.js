import React from 'react';
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { watchFetchImage } from './sagas'
import { reducer as image } from './reducers'
import favorites from './reducers/favorites'

import { NavigationContainer }  from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Theme from './theme'

import MainScreen       from './screens/Main'
import DetailsScreen    from './screens/Details'
import FavoritesScreen  from './screens/Favorites'

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  combineReducers({image, favorites}),
  applyMiddleware(sagaMiddleware)
);
sagaMiddleware.run(watchFetchImage);

const connectImageState = (Component => connect(state => ({...state.image, favorites: state.favorites}))(Component))
const connectFavoritesState = (Component => connect(state => ({favorites: state.favorites}))(Component))
  
const Stack = createStackNavigator();

export default function App () {
  return (
    <Provider store={store}>
      <NavigationContainer theme={Theme}>
        <Stack.Navigator>
          <Stack.Screen name={"Main"}       component={ connectImageState(MainScreen) } options={{headerShown: false}}/>
          <Stack.Screen name={"Favorites"}  component={ connectFavoritesState(FavoritesScreen) } />
          <Stack.Screen name={"Details"}    component={ DetailsScreen } />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}