import React from 'react';
import { StyleSheet, Text, ScrollView, Image, SafeAreaView } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import * as Linking from 'expo-linking';

import { useTheme } from '@react-navigation/native';

import Theme from '../theme'

import LikeButton from '../components/LikeButton'

const DetailsScreen = (props) => {

    const { colors } = useTheme()
    const data = props.route.params.data

    const Link = ({url}) => 
        <Text style={styles.url} onPress={()=>{Linking.openURL(url)}}>
            {url}
        </Text>

    return (
        <ScrollView style={{flex: 1}} >
            <StatusBar backgroundColor={colors.card} translucent={false} />
            <SafeAreaView>
                <Image style={StyleSheet.compose(styles.image, {aspectRatio: data.width/data.height})} resizeMode='contain' source={{ uri: data.download_url }} />
                <Text style={styles.description}>Author: {data.author}</Text>
                <Text style={styles.description}>Size: {data.width}×{data.height}</Text>
                <Text style={styles.description}>Link: <Link url={data.url} /></Text>
                <Text style={styles.description}>Download link: <Link url={data.download_url} /></Text>
                <LikeButton data={data}/>
            </SafeAreaView>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        flex: 0,
        width: '100%'
    },
    description: {
        color: Theme.colors.text,
        margin: 6
    },
    url: {
        textDecorationLine: 'underline',
        color: '#63a4ff'
    }
});

export default DetailsScreen