import React from 'react';
import { StyleSheet, Text, View, Image, SafeAreaView, Pressable, FlatList } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import { useTheme, useNavigation } from '@react-navigation/native';

const THUMBNAIL_SIZE = 256

const FavoritesScreen = (props) => {
  
  const { colors } = useTheme()
  const { navigate } = useNavigation();

  const Item = ( {data, thumbnail_url} ) => (
    <Pressable onPress={()=>{
        navigate('Details', {data: data})
    }}>
      <View style={styles.itemContainer}>
        <View style={styles.imageWrapper}>
          <Image resizeMode='contain' style={styles.thumbnail} source={{uri: thumbnail_url}}/>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title} adjustsFontSizeToFit>{data.id}</Text>
          <Text style={styles.description} numberOfLines={2}>{data.author + '\n'}{data.width}×{data.height}</Text>
        </View>
      </View>
    </Pressable>
  )

  const renderItem = ( { item, index } ) => {
    const thumbnail_url = item.download_url.split('/', 5).join('/') + '/' + THUMBNAIL_SIZE
    return (
      <Item data={item} thumbnail_url={thumbnail_url}/>
    )
  }

  return (
    <View style={{flex: 1}} >
      <StatusBar backgroundColor={colors.card} translucent={false} />
      <SafeAreaView>
        <FlatList
          keyExtractor={(item) => item.id}
          data={
            [...props.favorites.values()]
          }
          renderItem={renderItem}
          contentContainerStyle={{ flexGrow: 1 }}
        />
      </SafeAreaView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
      flex: 1
  },
  itemContainer: {
      flexDirection: 'row',
      alignItems: 'center'
  },
  imageWrapper: {
      width: 100,
      height: 100,
      margin: 8
  },
  thumbnail: {
      height: 100
  },
  textContainer: {
      flexDirection: 'column',
      flex: 1,
      margin: 8
  },
  title: {
      fontSize: 18,
      color: Theme.colors.text
  },
  description: {
      flexGrow: 1,
      color: Theme.colors.text
  }
})

export default FavoritesScreen