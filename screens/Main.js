import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, ActivityIndicator, SafeAreaView, Pressable } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import { useFocusEffect, useTheme }  from '@react-navigation/native';

import Theme from '../theme'

import { Button, IconButton } from '../components/specific'
import { FloatingMenu } from '../components'
import LikeButton from '../components/LikeButton'

import { fetchImage }   from '../actions'

const MainScreen = (props) => {

    const { colors } = useTheme()
    
    const [menuVisible, setMenuVisible] = useState(false);
  
    useFocusEffect(
        React.useCallback(() => {
            return () => setMenuVisible(false)
        }, [])
    );

    const Menu = (
        <FloatingMenu visible={menuVisible} onRequestClose={() => setMenuVisible(false)}>
            {props.data
                ? <Button listButton title='Details'
                    onPress={()=>{props.navigation.navigate('Details', {data: props.data})}}
                /> 
            : null}
            <Button listButton title='Favorites'
                onPress={()=>{props.navigation.navigate('Favorites')}}
            />
        </FloatingMenu>
    )

    const ImageBlock = (
        <>
            <View style={styles.imageBlockWrapper}>
                <Image style={styles.props} resizeMode='contain' source={{ uri: props.url }} />
                    { !props.dataError && props.data ? 
                        <Text style={styles.authorText}>by {props.data.author}</Text>
                    : null }
            </View>
            <LikeButton data={props.data}/>
        </>
    )
  
    return (
        <View style={{flex: 1}} >
            <StatusBar backgroundColor={colors.card} translucent={false} />
            <SafeAreaView style={styles.container}>
                <IconButton style={styles.moreButton} size={32} color={colors.card} name='more-vert' onPress={()=>{setMenuVisible(!menuVisible)}}/>
                {Menu}
                { props.loading
                    ? <ActivityIndicator style={{ flexGrow: 1 }} size='large' /> 
                    : props.imageError
                        ? <Text style={styles.placeholderText}>Error, try again</Text>
                : props.url
                    ? ImageBlock
                :  <View style={styles.placeholderText} /> }
                <Button title='Show Random Image' onPress={() => props.dispatch(fetchImage())} />
            </SafeAreaView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    moreButton: {
        alignSelf: 'flex-end'
    },
    placeholderText: {
        flexGrow: 1,
        textAlign: 'center',
        textAlignVertical: 'center'
    },
    imageBlockWrapper: {
        flexGrow: 1,
        justifyContent: 'flex-end'
    },
    props: {
        flex: 0,
        aspectRatio: 1
    },
    authorText: {
        color: Theme.colors.card,
        textAlign: 'right',
        fontSize: 12,
        margin: 6,
        marginRight: 8,
        right: 0
    }
});

export default MainScreen