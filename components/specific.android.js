import React from 'react';
import { StyleSheet, Pressable, Text } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons';

import Theme from '../theme'


const CustomButton = (props) => (
    <Pressable style={StyleSheet.compose(styles.buttonPressable, props.style)} android_ripple={{color: 'white', borderless: false}} onPress={props.onPress}>
        <Text style={StyleSheet.compose(styles.buttonText, props.textStyle)}>
            {props.title.toUpperCase()}
        </Text>
    </Pressable>
)

const IconButton = (props) => (
    <Pressable 
    style={StyleSheet.compose(styles.iconButtonPressable, props.style)}
    android_ripple={{color: 'white', borderless: false}}
        onPress={props.onPress}
        >
        <MaterialIcons style={StyleSheet.compose(styles.buttonIcon, props.iconStyle)}
            size={props.size || 32}
            color={props.color || Theme.colors.card}
            name={props.name}
            />
    </Pressable>
)

const styles = StyleSheet.create({
    buttonPressable: {
        backgroundColor: Theme.colors.card
    },
    buttonText: {
        margin: 8,
        textAlign: 'center',
        color: Theme.colors.text
    },
    iconButtonPressable: {
        flexShrink: 1,
        borderRadius: 30
    },
    buttonIcon: {
        margin: 8
    }
});

export { CustomButton as Button, IconButton }