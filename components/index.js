import React from 'react';
import { StyleSheet, Modal, TouchableWithoutFeedback, SafeAreaView, View } from 'react-native'

import Theme from '../theme'

const FloatingMenu = (props) => (
    <Modal
        animationType="fade"
        transparent={true}
        visible={props.visible}
        onRequestClose={props.onRequestClose}
    >
        <TouchableWithoutFeedback style={{flex: 1}} onPress={props.onRequestClose}>
        <SafeAreaView style={styles.backgroundWrapper}>
            <View style={styles.floatingContainer}>
            {props.children}
            </View>
        </SafeAreaView>
        </TouchableWithoutFeedback>
    </Modal>
)

const styles = StyleSheet.create({
    floatingContainer: {
        alignSelf: 'flex-end',
        margin: 8,
        borderRadius: 4
    },
    backgroundWrapper: {
        flexGrow: 1,
        position: 'relative'
    }
});

export { FloatingMenu }