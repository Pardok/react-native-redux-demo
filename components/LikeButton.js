import React, { useState } from 'react';
import { useTheme }  from '@react-navigation/native';
import { connect } from 'react-redux';

import { addWithID, removeByID } from '../actions/favorites'

import { IconButton } from '../components/specific'


const connectFavoritesState = (Component => connect(state => ({favorites: state.favorites}))(Component))

const LikeButton = connectFavoritesState(({ favorites, dispatch, data }) => {

    const { colors } = useTheme()

    const [liked, setLike] = useState(favorites.has(data.id));

    const updateLike = () => {
        if (liked != favorites.has(data.id)) {
            setLike(favorites.has(data.id))
        }
    }

    updateLike()
    
    const favorizeID = () => {
        dispatch(addWithID(data.id, data))
        updateLike()
    }

    const unfavorizeID = () => {
        dispatch(removeByID(data.id))
        updateLike()
    }

    return (
        <IconButton
            style={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}
            onPress={liked ? unfavorizeID : favorizeID}
            size={32}
            color={liked ? 'red' : colors.card}
            name={liked ? 'favorite' : 'favorite-border'}
        />
    )
})

export default LikeButton