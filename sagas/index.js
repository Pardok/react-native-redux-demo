import { Image } from 'react-native'
import { takeEvery, put, call } from 'redux-saga/effects'

import { requestImage, requestImageSuccess, requestImageError, requestImageData, requestImageDataSuccess, requestImageDataError } from '../actions'

const IMAGE_RES = 1000

function* watchFetchImage() {
    yield takeEvery('FETCHED_IMAGE', fetchImageAsync);
}
  
function* fetchImageAsync() {
    let url = ''
    // Load image
    try {
        yield put(requestImage());
        url = yield call(() => {
            return fetch('https://picsum.photos/' + IMAGE_RES)
                    .then(data => data.url)
            }
        );
        Image.prefetch(url)
        yield put(requestImageSuccess(url));
    } catch (error) {
        console.warn(error)
        yield put(requestImageError());
    }
    // Load image data
    try {
        yield put(requestImageData());
        const id = /\/id\/(.*?)\//.exec(url)[1]
        const data = yield call(() => {
            return fetch('https://picsum.photos/id/' + id + '/info')
                    .then(response => response.json())
            }
        );
        yield put(requestImageDataSuccess(data));
    } catch (error) {
        console.warn(error)
        yield put(requestImageDataError());
    }}

export { watchFetchImage }