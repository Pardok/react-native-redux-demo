const addWithID = (id, data) => {
    return { type: 'ADD_ID', id: id, data: data }
};

const removeByID = (id) => {
    return { type: 'REMOVE_ID', id: id }
};

const loadIDs = (ids) => {
    return { type: 'REMOVE_ID', ids: ids }
};

export { addWithID, removeByID, loadIDs }