const requestImage = () => {
    return { type: 'REQUESTED_IMAGE' }
};

const requestImageSuccess = (url) => {
    return { type: 'REQUESTED_IMAGE_SUCCEEDED', url: url }
};

const requestImageError = () => {
    return { type: 'REQUESTED_IMAGE_FAILED' }
};
const requestImageData = () => {
    return { type: 'REQUESTED_IMAGE_DATA' }
};

const requestImageDataSuccess = (data) => {
    return { type: 'REQUESTED_IMAGE_DATA_SUCCEEDED', data: data }
};

const requestImageDataError = () => {
    return { type: 'REQUESTED_IMAGE_DATA_FAILED' }
};

const fetchImage = () => {
    return { type: 'FETCHED_IMAGE' }
};

export { 
    requestImage, requestImageSuccess, requestImageError,
    requestImageData, requestImageDataSuccess, requestImageDataError,
    fetchImage
}