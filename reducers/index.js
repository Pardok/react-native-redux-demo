const initialState = {
    url: '',
    data: {},
    loading: false,
    imageError: false,
    dataError: false,
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUESTED_IMAGE':
            return {
                url: '',
                data: {},
                loading: true,
                imageError: false,
                dataError: false,
            };
        case 'REQUESTED_IMAGE_SUCCEEDED':
            return {
                url: action.url,
                data: {},
                loading: true,
                imageError: false,
                dataError: false,
            };
        case 'REQUESTED_IMAGE_FAILED':
            return {
                url: '',
                data: {},
                loading: false,
                imageError: true,
                dataError: false,
            };
        case 'REQUESTED_IMAGE_DATA':
            return {
                url: state.url,
                data: {},
                loading: true,
                imageError: false,
                dataError: false,
            };
        case 'REQUESTED_IMAGE_DATA_SUCCEEDED':
            return {
                url: state.url,
                data: action.data,
                loading: false,
                imageError: false,
                dataError: false,
            };
        case 'REQUESTED_IMAGE_DATA_FAILED':
            return {
                url: state.url,
                data: {},
                loading: false,
                imageError: false,
                dataError: true,
            };
        default:
        return state;
    }
};

export { reducer }