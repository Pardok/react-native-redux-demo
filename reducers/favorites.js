const initialState = new Map();
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_ID':
            return state.set(action.id, action.data);
        case 'REMOVE_ID':
            state.delete(action.id)
            return new Map([...state]);
        case 'LOAD_IDS':
            return new Set(action.ids);
        default:
        return state;
    }
};

export default reducer